package bdci0058MV.BBT;

import bdci0058MV.control.BibliotecaCtrl;
import bdci0058MV.repository.repoInterfaces.*;
import bdci0058MV.model.Carte;
import bdci0058MV.model.repo.CartiRepo;
import org.junit.*;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import bdci0058MV.util.Validator;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {
    Carte book;
    BibliotecaCtrl bibl;
    Validator validator;
    CartiRepoInterface cartiInterface;
    List<String> x = new ArrayList<>();
    List<String> y = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        cartiInterface = new CartiRepoInterface() {
            @Override
            public void adaugaCarte(Carte c) {

            }

            @Override
            public void modificaCarte(Carte nou, Carte vechi) {

            }

            @Override
            public void stergeCarte(Carte c) {

            }

            @Override
            public List<Carte> cautaCarte(String autor) {
                return null;
            }

            @Override
            public List<Carte> getCarti() {
                return null;
            }

            @Override
            public List<Carte> getCartiOrdonateDinAnul(String an) {
                return null;
            }
        };
                book = new Carte();
        bibl = new BibliotecaCtrl(cartiInterface);
        System.out.println("Before test!");
    }

    @After
    public void tearDown() throws Exception {
        book = null;
        bibl = null;
        System.out.println("After test");
    }

    @Test
    public void addCarte1() throws Exception {
        book = new Carte();
        String x1 = "Ana cu merele";
        String y1 = "mere";
        x.add(x1);
        y.add(y1);
        book.setTitlu("X");
        book.setAutori(x);
        book.setAnAparitie("1899");
        book.setEditura("abcdefghijklmnopqrs");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte2() throws Exception {
        book = new Carte();
        String x1 = "Ion";
        String y1 = "nici";
        x.add(x1);
        y.add(y1);
        book.setTitlu("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXa");
        book.setAutori(x);
        book.setAnAparitie("1855");
        book.setEditura("abcdefghijklmnopqrst");
        book.setCuvinteCheie(y);
        book.adaugaCuvantCheie("aici");
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte3() throws Exception {
        book = new Carte();
        String x1 = "Marian";
        String y1 = "gresit";
        x.add(x1);
        y.add(y1);
        book.setTitlu("ABCDEFGHIJKLMNOPQRSTUV");
        book.setAutori(x);
        book.setAnAparitie("2000");
        book.setEditura("");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test(expected = Exception.class)
    public void addCarte4() throws Exception {

        book = new Carte();
        String x1 = "HHDHA";
        String y1 = "bine";
        x.add(x1);
        y.add(y1);
        book.setTitlu("ABCDEFGHIJKLMNOPQRSTUVABCDEFGH");
        book.setAutori(x);
        book.setAnAparitie("1799");
        book.setEditura("abcdefghijkl");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test (expected = Exception.class)
    public void addCarte5() throws Exception {

        book = new Carte();
        String x1 = "Ana Are Mere";
        String y1 = "ana";
        x.add(x1);
        y.add(y1);
        book.setTitlu("");
        book.setAutori(x);
        book.setAnAparitie("2016");
        book.setEditura("A");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte6() throws Exception {

        book = new Carte();
        String x1 = "ghewugwe";
        String y1 = "jgas";
        x.add(x1);
        y.add(y1);
        book.setTitlu("AA");
        book.setAutori(x);
        book.setAnAparitie("2017");
        book.setEditura("ABCDEFGHIJKLMNOPQRSTU");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test (expected = Exception.class)
    public void addCarte7() throws Exception {

        book = new Carte();
        String x1 = "gfnjkqkbgnq";
        String y1 = "gjnewg";
        x.add(x1);
        y.add(y1);
        book.setTitlu("ABCDEFGHIJKLMNOPQRSTUABCDEFGHIJKLMNOPQRSTUijklmno");
        book.setAutori(x);
        book.setAnAparitie("2018");
        book.setEditura("AA");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }

    @Test
    public void addCarte8() throws Exception {
        book = new Carte();
        String x1 = "ghewbghwe";
        String y1 = "jej";
        x.add(x1);
        y.add(y1);
        book.setTitlu("ABCDEFGHIJKLMNOPQRSTUABCDEFGHIJKLMNOPQRSTUijklmnoA");
        book.setAutori(x);
        book.setAnAparitie("1800");
        book.setEditura("ABCDEFGHIJKLMNOPQRS");
        book.setCuvinteCheie(y);
        bibl.adaugaCarte(book);
    }
}