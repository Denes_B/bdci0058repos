package bdci0058MV.WBT;

import bdci0058MV.model.Carte;
import bdci0058MV.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    private Carte c1, c2, c3;
    CartiRepoMock cartiRepoMock;

    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c1.setTitlu("Poezii");
        c1.setAutori(Arrays.asList("Eminescu"));
        c1.setAnAparitie("2002");
        c1.setEditura("PoliromCluj-N");
        c1.setCuvinteCheie(Arrays.asList("Frumos","Toamna"));
        c2 = new Carte();
        c2.setTitlu("Povesti");
        c2.setAutori(Arrays.asList("Creanga"));
        c2.setAnAparitie("2002");
        c2.setEditura("PoliromCluj-N");
        c2.setCuvinteCheie(Arrays.asList("Copilarie","Amintiri"));

        c3 = new Carte();
        c3.setTitlu("Versuri");
        c3.setAutori(Arrays.asList("Minulescu"));
        c3.setAnAparitie("2002");
        c3.setEditura("PoliromCluj-N");
        c3.setCuvinteCheie(Arrays.asList("Efemer","Minciuna"));
        cartiRepoMock = new CartiRepoMock();


    }

    @After
    public void tearDown() throws Exception {

        c1 = c2 = c3 = null;
        cartiRepoMock = null;
    }

    @Test
    public void cautaCarteInBiblRepoGol() {

        assertEquals("Cautam carte in biblioteca goala",0,cartiRepoMock.cautaCarte("Eminescu").size());
        System.out.println("Repo gol verificat cu succes!");

    }
    @Test
    public void cautaCarteAutorExistent() {
cartiRepoMock.adaugaCarte(c1);
        cartiRepoMock.adaugaCarte(c2);

        assertEquals("Cautam autor existent",1,cartiRepoMock.cautaCarte("Eminescu").size());
        System.out.println("Autor existent gasit!");

    }

    @Test
    public void cautaCarteAutorInexistent() {
        cartiRepoMock.adaugaCarte(c3);

        assertEquals("Cautam autor inexistent",0,cartiRepoMock.cautaCarte("Eminescu").size());
        System.out.println("Autor inexistent!");

    }
}